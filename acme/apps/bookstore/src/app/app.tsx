import { Link, Route, Routes } from 'react-router-dom';
import { BooksFeature } from '@acme/books/feature';

import {
  GlobalStyles,
  Header,
  NavigationList,
  NavigationItem,
  Main,
} from '@acme/ui';

import { CartFeature } from '@acme/cart/feature';

export const App = () => {
  return (
    <>
      <GlobalStyles />
      <Header>
        <h1>Bookstore</h1>
        <NavigationList>
          <NavigationItem>
            <Link to="/books">Books</Link>
            <Link to="/cart">Cart</Link>
          </NavigationItem>
        </NavigationList>
      </Header>
      <Main>
        <Routes>
          <Route path="/books" element={<BooksFeature />} />
          <Route path="/cart" element={<CartFeature />} />
        </Routes>
      </Main>
    </>
  );
};

export default App;

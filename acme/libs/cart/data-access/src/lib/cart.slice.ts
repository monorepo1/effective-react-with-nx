import { ICartItem } from '@acme/shared-models';
import {
  createAsyncThunk,
  createEntityAdapter,
  createSelector,
  createSlice,
  EntityState,
  PayloadAction,
} from '@reduxjs/toolkit';
import { checkout } from './cart-data-access';

export const CART_FEATURE_KEY = 'cart';

export interface CartState extends EntityState<ICartItem> {
  cartStatus: 'ready' | 'pending' | 'ordered' | 'error'
  error: string
  order?: string
}

export const cartAdapter = createEntityAdapter<ICartItem>();

export const checkoutCart = createAsyncThunk<{order: string}, ICartItem[]>(
  'cart/checkoutStatus',
  (items: ICartItem[]) => checkout({items})
);

export const initialCartState: CartState = cartAdapter.getInitialState({
  cartStatus: 'ready',
  error: null,
});

export const cartSlice = createSlice({
  name: CART_FEATURE_KEY,
  initialState: initialCartState,
  reducers: {
    add: cartAdapter.addOne,
    remove: cartAdapter.removeOne,
  },
  extraReducers: (builder) => {
    builder
      .addCase(checkoutCart.pending, (state: CartState) => {
        state.cartStatus = 'pending'
      })
      .addCase(
        checkoutCart.fulfilled,
        (state: CartState, action: PayloadAction<ICartItem[]>) => {
          state.order = action.payload.order
          state.cartStatus = 'ordered'
        }
      )
      .addCase(checkoutCart.rejected, (state: CartState, action: PayloadAction<{error: Error}>) => {
        state.cartStatus = 'error';
        state.error = action.payload.error.message;
      });
  },
});

/*
 * Export reducer for store configuration.
 */
export const cartReducer = cartSlice.reducer;

/*
 * Export action creators to be dispatched. For use with the `useDispatch` hook.
 *
 * e.g.
 * ```
 * import React, { useEffect } from 'react';
 * import { useDispatch } from 'react-redux';
 *
 * // ...
 *
 * const dispatch = useDispatch();
 * useEffect(() => {
 *   dispatch(cartActions.add({ id: 1 }))
 * }, [dispatch]);
 * ```
 *
 * See: https://react-redux.js.org/next/api/hooks#usedispatch
 */
export const cartActions = cartSlice.actions;

/*
 * Export selectors to query state. For use with the `useSelector` hook.
 *
 * e.g.
 * ```
 * import { useSelector } from 'react-redux';
 *
 * // ...
 *
 * const entities = useSelector(selectAllCart);
 * ```
 *
 * See: https://react-redux.js.org/next/api/hooks#useselector
 */
const { selectAll } = cartAdapter.getSelectors();

export const getCartState = (rootState: unknown): CartState =>
  rootState[CART_FEATURE_KEY];

export const selectCartItems = createSelector(getCartState, selectAll);

export const selectCartStatus = createSelector(getCartState, (state: CartState) => state.cartStatus)

export const selectOrderNumber = createSelector(getCartState, (state: CartState) => state.order)

export const selectTotal = createSelector(selectCartItems, (items: ICartItem[]) => items.reduce((total, item) => total + item.cost, 0))

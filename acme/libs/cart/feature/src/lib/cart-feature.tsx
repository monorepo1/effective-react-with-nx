import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux'
import { selectOrderNumber } from '@acme/cart/data-access'
import { cartActions, checkoutCart, selectCartItems, selectCartStatus, selectTotal } from 'libs/cart/data-access/src';
import { Button } from '@acme/ui';

const StyledCartFeature = styled.div`
  .item {
    display: flex;
    align-items: center;
    padding-bottom: 9px;
    margin-bottom: 9px;
    border-bottom: 1px #ccc solid;
  }
  .description {
    flex: 1;
  }
  .cost {
    width: 10%;
  }
  .action {
    width: 10%;
  }
`;

export function CartFeature() {
  const dispatch = useDispatch()
  const order = useSelector(selectOrderNumber)
  const cartItems = useSelector(selectCartItems)
  const cartIsEmpty = cartItems.length === 0
  const total = useSelector(selectTotal)
  const status = useSelector(selectCartStatus)

  return (
    <StyledCartFeature>
      <h1>My Cart</h1>
      {
        order
          ? (
            <p>Your order number is <div>#{order}</div>.</p>
          )
          : (
            <>
            { cartIsEmpty ? <p>Your cart is empty</p> : null }
            <div>
              {
                cartItems.map(item => (
                  <div className="item" key={item.id}>
                    <span className='description'>{item.description}</span>
                    <span className='cost'>{item.cost.toFixed(2)}</span>
                    <span className="action">
                      <Button
                        onClick={() => dispatch(cartActions.remove(item.id))}
                      >remove</Button>
                    </span>
                  </div>
                ))
              }
            </div>
            <p>Total: $ {total.toFixed(2)}</p>
            <Button
              disabled={cartIsEmpty || status !== 'ready'}
              onClick={() => dispatch(checkoutCart(cartItems) as any)}
            >
              checkout
            </Button>
            </>
          )
      }
    </StyledCartFeature>
  );
}

export default CartFeature;
